package com.poc.converter.xmltocsv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.poc.converter.xmltocsv.config.AppConfig;

@SpringBootApplication
public class XmlToCsvApplication  implements CommandLineRunner{

	@Autowired
	private AppConfig apps;

	public static void main(String[] args) {
		SpringApplication.run(XmlToCsvApplication.class, args);
	}
	
	@Override
    public void run(String... args) {
		FileProcessor f1=new FileProcessor();
		
		f1.method1(apps);
        
    }

}
