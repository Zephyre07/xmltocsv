package com.poc.converter.xmltocsv.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
public class AppConfig {
	
	private String filepath;
	private String data;
	private String metadata;
	
	
	
	public String getFilepath() {
		return filepath;
	}



	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}



	public String getData() {
		return data;
	}



	public void setData(String data) {
		this.data = data;
	}



	public String getMetadata() {
		return metadata;
	}



	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}



	@Override
	public String toString() {
		return "AppConfig [filepath=" + filepath + ", data=" + data + ", metadata=" + metadata + "]";
	}

		

}
